package hotelReservation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import hotelReservation.entity.SignUp;
//import hotelReservation.model.SignUpModel;
import hotelReservation.service.HotelReservationService;

@Controller
public class HomeController {
	
	@Autowired
	HotelReservationService hotelReservationService;
	
//	SignUp signup;
	
//	@Autowired
//	SignUpModel signUpDetails;

	@RequestMapping("/")
	public String home() {
		return "user-Type";
	}

	@RequestMapping("/user-type")
	public String loginPageforUser() {
		return "user-login";
	}
	
	@RequestMapping("/user-signup")
	public String signup() {
		return "user-signup";
	}
	
	@RequestMapping(value= "/complete-signup", method = RequestMethod.POST)
	public String signupProcess(@ModelAttribute("user") SignUp signup) {
		
//		signup.setMobileNo(mobileNo);
//		signup.setName(userName);
//		signup.setPass(pass);
//		signup.setEmail(email);
		System.out.println(signup);
		hotelReservationService.save(signup);	
		return "user-login";
	}
//
//	@RequestMapping("/admin-type")
//	public String loginPageforAdmin() {
//		return "admin-login";
//	}
//	
//	public String signUp(@RequestParam("username") String userName,
//			@RequestParam("password") String pass, @RequestParam("email") String email, @RequestParam("mobileNo") Long mobileNo) {
//		signUpDetails.setEmail(email);
//		signUpDetails.setMobileNo(mobileNo);
//		signUpDetails.setName(userName);
//		signUpDetails.setPass(pass);
////		hotelReservationService.save(signUpDetails);
//		return "";
//	}
//
//	public String loginProcessforUser(@RequestParam("username") String userName,
//			@RequestParam("password") String pass) {
//		
//		
//		return "hotel-detail";
//	}
}
